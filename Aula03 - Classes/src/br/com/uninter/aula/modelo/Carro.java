package br.com.uninter.aula.modelo;

/**
 * Esta classe representa um carro no mundo real.
 * @author alunoanalise
 *
 */
public class Carro {
	
	// ATRIBUTOS
	private String placa;
	private String marca;
	private String cor;
	private float tanque;
	
	// CONSTRUTORES
	// construtores s�o metodos especiais chamados na cria��o dos objetos
	// possuem:
	//  - mesmo nome da classe 
	//  - n�o tem tipo de retorno 
	// Construtores devem inicializar os atributos
	
	public Carro(String placa, String marca, String cor){
		// this eh uma refer�ncia a propria classe
		System.out.println("Criando carro : "+ placa);
		this.placa = placa;
		this.marca = marca;
		this.cor = cor;
		this.tanque = 0;
	}
	
	
	// METODOS
	
	public void anda(){
		if (this.tanque > 0){
		System.out.println("Carro " +placa +" andando!");
		this.tanque -= 5;
		}
		else {
			System.out.println("Sem gasolina!!!");
		}
		
	}
	
	public void para(){
		System.out.println("Carro " +placa + " parado!");
	}
	
	public void abastecer (float quantidade){
		System.out.println("Carro " + placa + " sendo abastecido!");
		this.tanque += quantidade;
	}

}
